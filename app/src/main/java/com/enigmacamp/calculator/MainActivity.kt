package com.enigmacamp.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var firstOperand: String = ""
    var secondOperand: String = ""
    var operator: String = ""
    var operationResult: Double = 0.0
    var historyOperation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onNumberEvent(view: View){

        val selectedBtn = view as Button
        var btnClickValue: String = etShowNumber.text.toString()

        when(selectedBtn.id){
            btn0.id -> { btnClickValue = isZero("0", btnClickValue)}
            btn1.id -> { btnClickValue = isZero("1", btnClickValue)}
            btn2.id -> { btnClickValue = isZero("2", btnClickValue)}
            btn3.id -> { btnClickValue = isZero("3", btnClickValue)}
            btn4.id -> { btnClickValue = isZero("4", btnClickValue)}
            btn5.id -> { btnClickValue = isZero("5", btnClickValue)}
            btn6.id -> { btnClickValue = isZero("6", btnClickValue)}
            btn7.id -> { btnClickValue = isZero("7", btnClickValue)}
            btn8.id -> { btnClickValue = isZero("8", btnClickValue)}
            btn9.id -> { btnClickValue = isZero("9", btnClickValue)}
            btnDot.id -> { btnClickValue = when(btnClickValue.length){
                0 -> btnClickValue
                1 -> btnClickValue + "."
                else -> btnClickValue
            }}
            btnDelete.id -> {
                    btnClickValue = when(btnClickValue.length){
                        1 -> "0"
                        else -> btnClickValue.dropLast(1)
                    }
                }
            btnMinus.id -> {
                firstOperand = btnClickValue
                operator = "-"
                btnClickValue = resetTextToShow(btnClickValue)
            }
            btnPlus.id -> {
                firstOperand = btnClickValue
                operator = "+"
                btnClickValue = resetTextToShow(btnClickValue)
            }
            btnMul.id -> {
                firstOperand = btnClickValue
                operator = "*"
                btnClickValue = resetTextToShow(btnClickValue)
            }
            btnDiv.id -> {
                firstOperand = btnClickValue
                operator = "/"
                btnClickValue = resetTextToShow(btnClickValue)
            }
            btnEqual.id -> {
                secondOperand = btnClickValue
                btnClickValue = operation()
                operator = "="
                etShowNumber2.text = historyOperation
                historyOperation = ""
            }
            btnPersen.id -> {
                btnClickValue = (btnClickValue.toDouble()/100).toString()
            }
            btnAc.id -> {
                btnClickValue = "0"
                historyOperation = ""
                operator = ""
                etShowNumber2.text = historyOperation
            }
            else -> {
                operationResult = btnClickValue.toDouble()
            }
        }

        etShowNumber3.text = operator
        etShowNumber.text = btnClickValue
    }

    private fun resetTextToShow(btnClickValue: String): String {
        var btnClickValue1 = btnClickValue
        historyOperation += btnClickValue1 + operator
        btnClickValue1 = ""
        etShowNumber2.text = historyOperation
        return btnClickValue1
    }

    private fun operation(): String {
        historyOperation += secondOperand

        if(secondOperand != ""){
            if (operator == "+" ) {
                operationResult = firstOperand.toDouble() + secondOperand.toDouble()
            } else if (operator == "-") {
                operationResult = firstOperand.toDouble() - secondOperand.toDouble()
            } else if (operator == "*") {
                operationResult = firstOperand.toDouble() * secondOperand.toDouble()
            } else if (operator == "/") {
                operationResult = firstOperand.toDouble() / secondOperand.toDouble()
            }
        } else return  firstOperand

        return operationResult.toString()
    }

    fun isZero(value: String, prevValue : String): String{

       return when(prevValue){
            "0" -> value
            else -> prevValue+value
        }
    }
}
